let myLogger = (req, res, next) => {
    console.log(req.method, req.url, new Date().toDateString());
    next();
};
app.use(myLogger);

module.exports = {
    myLogger()
}