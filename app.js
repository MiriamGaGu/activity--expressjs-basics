const express = require('express')
const app = express()
const fs = require('fs')
// const appRouter = app.route()
require('dotenv').config()
let PORT = process.env.PORT || 3000;
// const myLogger = require('./function')

let myLogger = (req, res, next) => {
    console.log(req.method, req.url, new Date().toDateString());
    next();
};
app.use(myLogger);


app.get('/', (req, res) => {
    res.send(new Date().toDateString());
})
app.get('/about', (req, res) => {
    res.send(new Date().toDateString());
})
app.get('/api', (req, res) => {
    res.send(new Date().toDateString());
})

// server.listen(port, () => {
//     console.log('server listening on port ' + port)
// })

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`);
});
