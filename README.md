# Morgan Middleware

## Context

Middlewares are the key in Express.js, you need to keep practicing.

## The Assignment

Write your own `morgan` logger.

### About the Design

The middleware should print the next sequence:


+ If I am visiting the root route
  ```sh
  GET / - Wed Jan 23 2019
  ```
+ If I am visiting the /about route
  ```sh
  GET /about - Wed Jan 23 2019
  ```
+ If I am visiting the /api route
  ```sh
  GET /api - Wed Jan 23 2019
  ```

**HINT: Don’t hardcode information. You must obtain method (GET), url (/, /api, /about) and date dynamically.**

### Setup Instructions

+ Install and configure a basic `Express.js` application.
+ Write your own **Morgan** middleware.
+ Get fancy and print the date with any other color using `chalk`.

### Deliverables

+ A GitHub repository
+ You will need to push your code to github

```sh
git add .
git commit -m "«your commit msg»"
git push origin master
```

using PORT=VALUE at dotenv and using it at app.js